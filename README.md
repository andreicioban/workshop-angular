## Get started

1. Clone the repo: `git git@bitbucket.org:andreicioban/workshop-angular.git`
2. Install dependencies: `npm install`
5. Start the server: `node app.js`

### Authenticating

Make a `POST` to `http://localhost:9000/api/authenticate` with username and password parameters as `x-www-form-urlencoded` or `RAW body`. 

```
  {
    email: 'andrei',
    password: 'password'
  }
```
Store the token.

### Verifying a Token and Listing Users

Make a `GET` request to `http://localhost:9000/api/users` with the toke as:

 1. header parameter of `x-access-token`.
 2. body property `toke`
 3. URL parameter `token`
 
 To create a new user:
 Make `POST` request to `http://localhost:9000/api/users` with:

```
  {
    email: 'newusername',
    password: 'newpassword',
    isAdmin: Boolean
  }
```
 
Make a `GET` request to `http://localhost:9000/api/check` to decode your token
 
### Random
 
 Make a `GET` request to `http://localhost:9000/api/randomReflection` to get 'life advices'
 
 Make a `GET` request to `http://localhost:9000/api/randomFortune` to know your fortune