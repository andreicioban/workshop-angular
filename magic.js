/**
 * Created by Andrei on 3/3/2016.
 */
var config      = require('./config');
var unirest      = require('unirest');

function getRandomCow(cb){
  unirest.get("https://thibaultcha-fortunecow-v1.p.mashape.com/random")
    .header("X-Mashape-Key", config.apiKey)
    .header("Accept", "text/plain")
    .end(function (result) {
      console.log(result.body);
      cb(result.body);
    });
}

function getRandomFortune(cb){
  unirest.get("https://thibaultcha-fortunecow-v1.p.mashape.com/")
    .header("X-Mashape-Key", config.apiKey)
    .header("Accept", "text/plain")
    .end(function (result) {
      console.log(result.body);
      cb(result.body);
    });
}

module.exports.getRandomCow = getRandomCow;
module.exports.getRandomFortune = getRandomFortune;