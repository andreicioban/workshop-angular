var express 	  = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mongoose    = require('mongoose');
var config 		  = require('./config');
var User   		  = require('./app/models/user');
var auth        = require('./app/auth');
var cors = require('cors');

var port = process.env.PORT || 9000;

mongoose.connect(config.database);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

app.use(morgan('dev'));

app.get('/init', function(req, res) {
  User.count({}, function(err, c) {
    if(c === 0){
      var salt = auth.createSalt();

      var me = new User({
        name: 'Andrei Cioban',
        email: 'andrei.cioban@assist.ro',
        salt: salt,
        password: auth.hashPwd(salt, 'Assist123'),
        isAdmin: true
      });

      me.save(function(err) {
        if (err) throw err;
        console.log('User saved successfully');
        res.json({ success: true });
      });

    }else{
      res.json({message: 'DB not empty', success: false});
    }
  });
});

app.get('/', function(req, res) {
	res.send('Much working. Such fast!');
});

app.use('/api', require('./private-routes'));

app.listen(port);
console.log('Such started, WOW port:' + port);
