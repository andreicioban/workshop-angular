/**
 * Created by Andrei on 3/3/2016.
 */
var express 	  = require('express');
var jwt         = require('jsonwebtoken');
var _         = require('lodash');
var User        = require('./app/models/user');
var config      = require('./config');
var auth        = require('./app/auth');
var magic        = require('./magic');
var apiRoutes   = express.Router();

apiRoutes.post('/authenticate', function(req, res) {

    User.findOne({
        email: req.body.email
    }, function(err, user) {

        if (err) throw err;

        if (!user) {
            res.status(403).json({ success: false, message: 'Authentication much failed. User such not found.' });
        } else if (user) {
            if (user.password !== auth.hashPwd(user.salt, req.body.password)) {
                res.status(403).json({ success: false, message: 'Such failed. Wrong password.' });
            } else {
                var token = jwt.sign(user, config.secret, {
                    expiresIn: 86400
                });
                res.json({
                    success: true,
                    message: 'Such encryption. Much token!',

                    token: token
                });
            }
        }
    });
});

apiRoutes.use(function(req, res, next) {
    var token = req.headers['x-access-token'] || req.body.token || req.param('token');
    if (token) {
        jwt.verify(token, config.secret, function(err, decoded) {
            if (err) {
                return res.status(403).json({ success: false, message: 'Much failed to authenticate token.' });
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        return res.status(403).send({
            success: false,
            message: 'No token provided. Much bad.'
        });
    }
});

apiRoutes.get('/', function(req, res) {
    res.json({ message: 'Much private!' });
});

apiRoutes.get('/users', function(req, res) {
    var query = req.decoded._doc.isAdmin ? {} : {isAdmin: {$not: { $eq: true}}};
    User.find(query, function(err, users) {
        res.json(users);
    });
});

apiRoutes.get('/check', function(req, res) {
    res.json(req.decoded._doc);
});

//Creare user
apiRoutes.post('/users', function (req, res){
  var salt = auth.createSalt();

  if(!(!!req.body.password)){
    res.send('Much Missing password. Such fail.');
  }

  var newUser=new User({
      name: req.body.name,
      email: req.body.email,
      salt: salt,
      password: auth.hashPwd(salt, req.body.password),
      isAdmin: req.decoded._doc.isAdmin ? req.body.isAdmin : false
  });

  newUser.save(function (err) {
      if (!err) {
          console.log("User such created");
          res.send(newUser);
      } else {
          console.log(err);
          res.send('Much error. Such BOOM.'+err.message);
      }
  });
});

//update user
apiRoutes.put('/users', function (req, res){
  User.findById(req.body._id, function (err, user){
    if (user) {
      user = _.extend(user, req.body);
      user.isAdmin = req.decoded._doc.isAdmin ? req.body.isAdmin : user.isAdmin;
      user.save();
      res.send(user);
    } else {
      res.send('User such not found at update');
    }
  });
});

apiRoutes.get('/randomReflection', function(req, res) {
    if(req.decoded._doc.isAdmin){
      magic.getRandomFortune(function(fortune){
        res.json({data: fortune});
      });
    }else{
        res.status(403).send('Such Unauthorised. Much hacker')
    }
});

apiRoutes.get('/randomFortune', function(req, res) {
  magic.getRandomCow(function(fortune){
    res.json({data: fortune});
  });
});

module.exports = apiRoutes;